public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int first, int second) {
        if(first < 10 || second < 10) {
            return -1;
        }
        int biggest = Math.max(first, second);
        int divisor = 2;
        int greatestCommonDivisor = 1;
        while(divisor <= biggest) {
            if(first % divisor == 0 && second % divisor == 0) {
                greatestCommonDivisor = divisor;
            }
            divisor++;
        }
        return greatestCommonDivisor;
    }
}
