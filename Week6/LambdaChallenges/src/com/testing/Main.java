package com.testing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        // Change the following code to lambda expressions
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String myString = "Let's split this up into an array";
                String[] parts = myString.split(" ");
                for (String part: parts) {
                    System.out.println(part);
                }
            }
        };

//        String myString = "Let's split this up into an array";
//        Consumer<String> splitting = s ->
//                Arrays.stream(s.split(" "))
//                .forEach(System.out::println);
//        splitting.accept(myString);

        Runnable runnable1 = () -> {
            String myString = "Let's split this up into an array";
            String[] parts = myString.split(" ");
            for (String part: parts) {
                System.out.println(part);
            }
        }; // Course answer

        System.out.println("*********************************");

        Function<String, String>  everySecondChar_lambda = s -> {
            StringBuilder returnVal = new StringBuilder();
            for(int i = 0; i < s.length();i++) {
                if (i % 2 == 1) {
                    returnVal.append(s.charAt(i));
                }
            }
            return returnVal.toString();
        };

        System.out.println(everySecondChar_lambda.apply("1234567890"));

        System.out.println("*********************************");

        System.out.println(everySecondChar("1234567890", everySecondChar_lambda));

        System.out.println("*********************************");

        // Write a lambda expression that maps to the java.util.Supplier interface, it should
        // return the string "I love Java!", assign it to a variable called iLoveJava

        Supplier<String> iLoveJava = () -> "I love Java!";

        // Assign the result to supplierResult and print
        String supplierResult = iLoveJava.get();
        System.out.println(supplierResult);

        System.out.println("*********************************");

        // Use streams and chained functions
        List<String> names = Arrays.asList(
                "Amelia", "Olivia", "emily", "Isla", "Ava", "oliver", "Jack", "Charlie", "harry", "Jacob"
        );

        names.stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .sorted()
                .forEach(System.out::println);

        System.out.println("*********************************");

        List<String> topNames2015 = Arrays.asList(
                "Amelia", "Olivia", "emily", "Isla", "Ava", "oliver", "Jack", "Charlie", "harry", "Jacob"
        );

        //Change code to use method references
        List<String> firstUpperCaseList = new ArrayList<>();
        topNames2015.forEach(name ->
                firstUpperCaseList.add(name.substring(0,1).toUpperCase() + name.substring(1)));
        firstUpperCaseList.sort(String::compareTo);
        firstUpperCaseList.forEach(System.out::println);

        System.out.println("*********************************");

        // Modify code to print count names starting with A
        List<String> names2 = Arrays.asList(
                "Amelia", "Olivia", "emily", "Isla", "Ava", "oliver", "Jack", "Charlie", "harry", "Jacob"
        );

        long countStartingA = names2.stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .filter(s -> s.startsWith("A"))
                .count();

        System.out.println("Number of names starting with A: " + countStartingA);

    }

    // Write the following method as a lambda expression
    public static String everySecondChar(String source) {
        StringBuilder returnVal = new StringBuilder();
        for(int i = 0; i < source.length();i++) {
            if (i % 2 == 1) {
                returnVal.append(source.charAt(i));
            }
        }

        return returnVal.toString();
    }

    // Write the previous method as one that accepts a function as a parameter
    public static String everySecondChar(String s, Function<String, String> lambdaFunction) {
        return lambdaFunction.apply(s);
    }
}
