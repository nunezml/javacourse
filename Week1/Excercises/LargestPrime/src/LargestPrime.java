public class LargestPrime {

    public static int getLargestPrime(int number) {
        int largestFactor = -1;
        boolean isPrime;
        if(number > 1) {
            for(int i=2;i<=number;i++) {
                // Check if number is prime
                isPrime = true;
                for(int j=2;j<i;j++){
                    if(i % j == 0) {
                        isPrime = false;
                        break;
                    }
                }
                // Get greatest factor
                if(isPrime) {
                    if(number % i == 0) {
                        largestFactor = i;
                    }
                }
            }
        }
        return largestFactor;
    }
}
