package academy.learnprogramming;

import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team> {
    private String name;
    private ArrayList<T> teams = new ArrayList<>();

    public League(String name) {
        this.name = name;
    }

    public boolean addTeam(T newTeam) {
        if(teams.isEmpty()) {
            teams.add(newTeam);
            return true;
        } else {
            if (teams.contains(newTeam)) {
                System.out.println("Team " + newTeam.getName() + " is already in the league.");
                return false;
            } else {
                teams.add(newTeam);
                return true;
            }
        }
    }

    public void showLeagueTable() {
        Collections.sort(teams);
        for(T team: teams) {
            System.out.println(team.getName() + ": " + team.ranking());
        }
    }
}
