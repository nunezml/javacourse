package academy.learnprogramming;

public class DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double num1, double num2) {
        // Get 3 decimal places
        int num1_3dec = (int) (num1 * 1000.0);
        int num2_3dec = (int) (num2 * 1000.0);
        System.out.println(num1_3dec);
        return (num1_3dec == num2_3dec);
    }
}
