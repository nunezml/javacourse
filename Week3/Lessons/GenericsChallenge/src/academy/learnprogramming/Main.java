package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        // Create a generic class to implement a league table for a sport.
        // The class should allow teams to be added to the list, and store
        // a list of teams that belong to the league.
        //
        // Your class should have a method to print out the teams in order,
        // with the team at the top of the league printed first.
        //
        // Only teams of the same type should be added to any particular
        // instance of the league class - the program should fail to compile
        // if an attempt is made to add an incompatible team.

        // Create soccer teams
        SoccerPlayer soccerPlayer = new SoccerPlayer("Tim");
        Team<SoccerPlayer> soccerTeam1 = new Team<>("Soccer Team 1");
        soccerTeam1.addPlayer(soccerPlayer);

        Team<SoccerPlayer> soccerTeam2 = new Team<>("Soccer Team 2");
        soccerTeam2.addPlayer(soccerPlayer);

        Team<SoccerPlayer> soccerTeam3 = new Team<>("Soccer Team 3");
        soccerTeam3.addPlayer(soccerPlayer);

        Team<SoccerPlayer> soccerTeam4 = new Team<>("Soccer Team 4");
        soccerTeam4.addPlayer(soccerPlayer);

        // Create league
        League<Team<SoccerPlayer>> soccerLeague = new League<>("Soccer League");
        soccerLeague.addTeam(soccerTeam1);
        soccerLeague.addTeam(soccerTeam2);
        soccerLeague.addTeam(soccerTeam3);
        soccerLeague.addTeam(soccerTeam4);

        // Play games
        soccerTeam1.matchResult(soccerTeam2, 2, 1);
        soccerTeam1.matchResult(soccerTeam3, 3, 6);
        soccerTeam1.matchResult(soccerTeam4, 2, 3);
        soccerTeam2.matchResult(soccerTeam3, 2, 1);
        soccerTeam2.matchResult(soccerTeam4, 2, 1);
        soccerTeam3.matchResult(soccerTeam4, 0, 0);

        // Show league table
        soccerLeague.showLeagueTable();

    }
}
