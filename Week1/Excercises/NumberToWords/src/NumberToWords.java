public class NumberToWords {

    public static void numberToWords(int number) {
        if(number < 0 ) {
            System.out.println("Invalid Value");
        } else {
            int reversedNumber = reverse(number);
            int numberDigits = getDigitCount(number);
            int reversedDigits = getDigitCount(reversedNumber);

            do {
                int digit = reversedNumber % 10;
                reversedNumber /= 10;
                switch (digit) {
                    case 0:
                        System.out.println("Zero");
                        break;
                    case 1:
                        System.out.println("One");
                        break;
                    case 2:
                        System.out.println("Two");
                        break;
                    case 3:
                        System.out.println("Three");
                        break;
                    case 4:
                        System.out.println("Four");
                        break;
                    case 5:
                        System.out.println("Five");
                        break;
                    case 6:
                        System.out.println("Six");
                        break;
                    case 7:
                        System.out.println("Seven");
                        break;
                    case 8:
                        System.out.println("Eight");
                        break;
                    case 9:
                        System.out.println("Nine");
                        break;
                    default:
                        System.out.println("Invalid digit found");
                }
            } while(reversedNumber != 0);

            if (numberDigits > reversedDigits) {
                for(int i=0;i<numberDigits-reversedDigits;i++)
                    System.out.println("Zero");
            }
        }


    }

    public static int getDigitCount(int number) {
        if(number < 0 ) return -1;
        int digitsCount = 0;
         do {
            int digit = number % 10;
            number /= 10;
            digitsCount++;
        } while(number != 0);
        return digitsCount;
    }

    public static int reverse(int number) {
        int copyOfNumber = Math.abs(number);
        int reverse = 0;
        while (copyOfNumber > 0) {
            // extract the least-significant digit
            int digit = copyOfNumber % 10;
            reverse *= 10;
            reverse += digit;
            copyOfNumber /= 10;
        }
        return reverse * (number < 0 ? -1 : 1);
    }
}
