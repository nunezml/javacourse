package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        System.out.println(getDurationString(65, 45));
    }

    public static String getDurationString(long minutes, long seconds) {
        if (minutes < 0 || seconds < 0 || seconds > 59) {
            return "Invalid Value";
        }
        long hours = minutes / 60;
        int newMinutes = (int) (minutes % 60) ;

        return hours + "h " + newMinutes + "m " + seconds + "s";
    }

    public static String getDurationString(long seconds) {
        if (seconds) {
            return "Invalid Value";
        }
        long minutes = seconds / 60;
        int newSeconds = (int) (seconds % 60) ;

        return getDurationString(minutes, newSeconds);
    }
}
