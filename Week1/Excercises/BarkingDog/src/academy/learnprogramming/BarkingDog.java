package academy.learnprogramming;

public class BarkingDog {
    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if(hourOfDay < 0 || hourOfDay > 23) {
            System.out.println("Hour of the day out of range");
            return false;
        }

        return barking && (hourOfDay < 8 || hourOfDay > 22);

    }
}
