package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        /*
        Your job is to create a simple banking application
        There should be a Bank class
        It should have an arraylist of Branches
        Each Branch should have an arrayList of Customers
        The Customer class should have an arraylist of Doubles (transactions)
        Customer: Name, and the ArrayList of doubles.
        Branch: Needs to be able to add a new customer and initial transaction amount. Also needs to add additional
        transactions for that customer/branch.
        Bank: Add a new branch, add a customer to that branch with initial transaction. Add a transaction for an
        existing customer for that branch. Show a list of customers for a particular branch and optionally a list
        of their transactions.
        Demonstrate autoboxing and unboxing in your code
        Hint: Transactions
        Add data validation
        e.g. Check if exists, or does not exist, etc
         */

        Bank bank = new Bank("National Bank");

        bank.addBranch("Branch1");
        bank.addCustomer("Branch1", "Tom", 50.0);
        bank.addCustomer("Branch1", "John", 125.3);
        bank.addCustomer("Branch1", "Mark", 69.0);

        bank.addBranch("Branch2");
        bank.addCustomer("Branch2", "Peter", 250.0);

        bank.addCustomerTransaction("Branch1", "Tom", 42.22);
        bank.addCustomerTransaction("Branch1", "Tom", 12.44);
        bank.addCustomerTransaction("Branch1", "Mark", 1.65);

        bank.listCustomers("Branch1", true);
        bank.listCustomers("Branch2", true);

        if(!bank.addCustomer("Branch3", "Bob", 5.35)){
            System.out.println("Error Branch3 does not exist");
        }
    }
}
