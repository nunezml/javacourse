package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        int value = 1;
        if(value == 1) {
            System.out.println("Value was 1");
        } else if (value == 2) {
            System.out.println("Value was 2");
        } else {
            System.out.println("Value was not 1 or 2");
        }

        int switchValue = 1;
        switch(switchValue) {
            case 1:
                System.out.println("Value was 1");
                break;

            case 2:
                System.out.println("Value was 2");
                break;

            case 3:case 4: case 5:
                System.out.println("Value was 3, or 4, or 5");
                System.out.println("Actually it was a " + switchValue);

             default:
                System.out.println("Value was not 1, 2, 3, 4 or 5");
                break;
        }

        // Create a new switch statement using char instead of int
        // create a new char variable
        // create a switch statement testing for A, B, C, D or E
        // display a message if any of these are found and then break
        // Add a default which displays a message saying not found

        char charSwitch = 'A';
        switch(charSwitch) {
            case 'A':
                System.out.println("Found char A");
                break;

            case 'B':
                System.out.println("Found char B");
                break;

            case 'C': case 'D':case 'E':
                System.out.println("Found char " + charSwitch);
                break;

            default:
                System.out.println("Char " + charSwitch + " not found");
                break;
        }

        //

        String month = "JaNuArY";
        switch(month.toLowerCase()) {
            case "january":
                System.out.println("Jan");
                break;
            case "june":
                System.out.println("Jun");
                break;
            default:
                System.out.println("Not sure");
        }

    }
}
