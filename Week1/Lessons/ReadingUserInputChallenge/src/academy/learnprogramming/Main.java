package academy.learnprogramming;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int count = 0;
        Scanner scanner = new Scanner(System.in);
        int number;
        int sum = 0;
        while(count < 10){
            System.out.println("Enter number #" + (count+1) + ":");
            if(scanner.hasNextInt()) {
                number = scanner.nextInt();
                count++;
                sum += number;
            } else {
                System.out.println("Input is not a number");
            }
            scanner.nextLine();
        }

        System.out.println("Sum of numbers = " + sum);
        scanner.close();
    }
}
