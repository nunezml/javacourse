package academy.learnprogramming;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String[] strArray = new String[10];
        int[] intArray = new int[10];

        ArrayList<String> strArrayList = new ArrayList<String>();
        strArrayList.add("Tim");

        ArrayList<Integer> intArrayList = new ArrayList<Integer>();
        // Boxing: primitive int 54 to Integer 54
        Integer integer = new Integer(54); //deprecated
        for(int i=0;i<10;i++) {
            intArrayList.add(Integer.valueOf(i));
        }

        // Unboxing: from Integer to int using intValue()
        for(int i=0;i<10;i++) {
            System.out.println(i + " --->" + intArrayList.get(i).intValue());
        }

        // Autoboxing: Java boxes in compile time
        Integer myIntValue = 56; // Integer.valueOf(56)
        int myInt = myIntValue; // myIntValue.intValue();

        // ************************************************

        ArrayList<Double> myDoubleValues = new ArrayList<Double>();
        for(double dbl=0.0; dbl<=10.0;dbl += 0.5) {
            myDoubleValues.add(Double.valueOf(dbl));
        }

        for(int i=0;i<myDoubleValues.size();i++) {
            double value = myDoubleValues.get(i).doubleValue();
            System.out.println(i + " ---> " + value);
        }

    }
}
