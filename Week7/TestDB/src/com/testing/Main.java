package com.testing;

import javax.swing.plaf.nimbus.State;
import java.sql.*;

public class Main {
    public static final String DB_NAME = "testjava.db";
    public static final String CONNECTION_STRING =
            "jdbc:sqlite:C:\\Users\\Letto\\Desktop\\Git\\javacourse\\Week7\\TestDB\\databases\\" + DB_NAME;
    public static final String TABLE_CONTACTS = "contacts";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";

    public static void main(String[] args) {
//	    try (Connection conn = DriverManager.getConnection(
//                "jdbc:sqlite:C:\\Users\\Letto\\Desktop\\Git\\javacourse\\Week7\\TestDB\\databases\\testjava.db");
//             Statement statement = conn.createStatement();
//             ) { // To automatically close resources

        try {
//            Connection conn = DriverManager.getConnection(
//                    "jdbc:sqlite:C:\\Users\\Letto\\Desktop\\Git\\javacourse\\Week7\\TestDB\\databases\\testjava.db");
//            conn.setAutoCommit(false); // statements won't auto commit
//            Statement statement = conn.createStatement();
//            statement.execute("CREATE TABLE IF NOT EXISTS contacts" +
//                                " (name TEXT, phone INTEGER, email TEXT)");
//            statement.execute("INSERT INTO contacts (name, phone, email)" +
//                    "VALUES('Joe', 4564879, 'joe@mail.com')");
//            statement.execute("INSERT INTO contacts (name, phone, email)" +
//                    "VALUES('Jane', 6568789, 'jane@mail.com')");
//            statement.execute("INSERT INTO contacts (name, phone, email)" +
//                    "VALUES('Fido', 456645, 'fido@mail.com')");
//
//            statement.execute("UPDATE contacts SET phone=5564646 WHERE name='Jane'");

//            statement.execute("SELECT * FROM contacts");

            //********************************************************

            Connection conn = DriverManager.getConnection(CONNECTION_STRING);
            Statement statement = conn.createStatement();

            statement.execute("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

            //CREATE
            statement.execute("CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS +
                    " (" + COLUMN_NAME + " text, " +
                            COLUMN_PHONE + " integer, " +
                            COLUMN_EMAIL + " text" +
                    ")");
//            statement.execute("INSERT INTO " + TABLE_CONTACTS +
//                    "(" + COLUMN_NAME + ", " +
//                            COLUMN_PHONE + ", " +
//                            COLUMN_EMAIL +
//                    ")" +
//                    "VALUES('Tim', 4564879, 'tim@mail.com')");
//            statement.execute("INSERT INTO " + TABLE_CONTACTS +
//                    "(" + COLUMN_NAME + ", " +
//                            COLUMN_PHONE + ", " +
//                            COLUMN_EMAIL +
//                    ")" +
//                    "VALUES('Joe', 4564879, 'joe@mail.com')");
//            statement.execute("INSERT INTO " + TABLE_CONTACTS +
//                    "(" + COLUMN_NAME + ", " +
//                            COLUMN_PHONE + ", " +
//                            COLUMN_EMAIL +
//                    ")" +
//                    "VALUES('Jane', 6568789, 'jane@mail.com')");
            insertContact(statement, "Tim", 654987, "tim@mail.com");
            insertContact(statement, "Joe", 789123, "joe@mail.com");
            insertContact(statement, "Jane", 784234, "jane@mail.com");
            insertContact(statement, "Fido", 7874524, "fido@mail.com");


            //UPDATE
            statement.execute("UPDATE " + TABLE_CONTACTS + " SET " +
                                COLUMN_PHONE + "=65456465" +
                                " WHERE " + COLUMN_NAME + "='Jane'");

            //DELETE
            statement.execute("DELETE FROM " + TABLE_CONTACTS +
                    " WHERE " + COLUMN_NAME + "='Joe'");

            //READ
            ResultSet results = statement.executeQuery("SELECT * FROM " + TABLE_CONTACTS);
            while(results.next()) {
                System.out.println(results.getString(COLUMN_NAME) + " " +
                        results.getInt(COLUMN_PHONE) + " " +
                        results.getString(COLUMN_EMAIL));
            }
            results.close();

            // First close statements, then close connection
            statement.close();
            conn.close();

        } catch(SQLException e) {
            System.out.println("Something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void insertContact(Statement statement, String name, int phone, String email) throws SQLException {
        statement.execute("INSERT INTO " + TABLE_CONTACTS +
                "(" + COLUMN_NAME + ", " +
                COLUMN_PHONE + ", " +
                COLUMN_EMAIL +
                ")" +
                "VALUES('" + name + "', " + phone + ", '" + email + "')");
    }
}
