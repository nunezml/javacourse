package academy.learnprogramming;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Button btnPrint = new Button("Print");

    public static void main(String[] args) {
	    Gearbox mcLarren = new Gearbox(6);
        mcLarren.addGear(1, 5.3);
        mcLarren.addGear(3, 10.6);
        mcLarren.addGear(3, 15.9);
        mcLarren.operateClutch(true);
        mcLarren.changeGear(1);
        mcLarren.operateClutch(false);
        System.out.println(mcLarren.wheelSpeed(1000));
        mcLarren.changeGear(2);
        System.out.println(mcLarren.wheelSpeed(3000));
        mcLarren.operateClutch(true);
        mcLarren.changeGear(3);
        mcLarren.operateClutch(false);
        System.out.println(mcLarren.wheelSpeed(6000));

        // ***************************************************
        // local class
//        class ClickListener implements Button.OnClickListener {
//            public ClickListener() {
//                System.out.println("I've been attached");
//            }
//
//            @Override
//            public void onClick(String title) {
//                System.out.println(title + " was clicked");
//            }
//        }
//        btnPrint.setOnClickListener(new ClickListener());

        // anonimous class
        btnPrint.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(String title) {
                System.out.println(title + " was clicked");
            }
        });
    }

    private static void listen() {
        boolean quit = false;
        while(!quit) {
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch(choice) {
                case 0:
                    quit = true;
                    break;
                case 1:
                    btnPrint.onClick();
            }
        }
    }
}
