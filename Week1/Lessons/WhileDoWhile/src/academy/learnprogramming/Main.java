package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        int count = 0;

        while(count != 5) {
            System.out.println("Count value is " + count);
            count++;
        }

        for(count=0; count<5; count++) {
            System.out.println("Count value is " + count);
        }

        count = 0;
        while(true) {
            if(count == 5) {
                break;
            }
            System.out.println("Count value is " + count);
            count++;
        }

        do {
            System.out.println("Count values was " +count);
            count++;
            if(count>100){
                break;
            }
        } while (count != 5);

        int number = 4;
        int finishNumber = 20;

        while(number <= finishNumber) {
            number++;
            if(!isEvenNumber(number)) {
                continue;
            }

            System.out.println("Even number " + number);
        }

        // Modify the while code above
        // Make it so also records the total number of even numbers it has found and break once 5 are found
        // and at the end, display the total number of even numbers found

        int number = 4;
        int finishNumber = 20;
        int countEvenNumbers = 0;

        while(number <= finishNumber) {
            number++;
            if (!isEvenNumber(number)) {
                continue;
            }

            System.out.println("Even number " + number);
            countEvenNumbers++;
            if (countEvenNumbers == 5) break;
        }
        System.out.println("Total even numbers found " + countEvenNumbers);
    }

    // Create a method called isEvenNumber that takes a parameter of type int
    // Its purpose is to determine if the argument passed to the method is an even number or not.
    // return true if an even number, otherwise return false;
    public static boolean isEvenNumber(int number) {
        return number % 2 == 0;
    }
}
