package com.testing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String string = "I am a String. Yes, I am";
        System.out.println(string);
        String yourString = string.replaceAll("I", "You"); // Method receives a regular expression
                                                                        // with the pattern to replace in the String
        System.out.println(yourString);

        String alphanumeric = "abcDeeeF12Ghhiiiiijk199z";
        System.out.println(alphanumeric.replaceAll(".", "Y"));
        // Character class are wildcards -> dot character means all contents

        System.out.println(alphanumeric.replaceAll("^abcDeee", "YYY")); // Only matches at the start

        String secondString = "abcDeeeF12GhhiiiiijabcDeeek1992";
        System.out.println(secondString.replaceAll("^abcDeee", "YYY"));

        System.out.println(alphanumeric.matches("^hello"));
        System.out.println(alphanumeric.matches("abcDeee")); // Matches checks if the whole regular expression
                                                                    // matches the string
        System.out.println(alphanumeric.matches("abcDeeeF12GhhiiiiijabcDeeek1992"));

        System.out.println(alphanumeric.replaceAll("ijk199z$", "THE END"));
        System.out.println(alphanumeric.replaceAll("[aei]", "X")); // replaces each a, e or i
        System.out.println(alphanumeric.replaceAll("[aei][Fj]", "X"));
                                                // Replaces each a,e or i followed by an F or j

        System.out.println("harry".replaceAll("[Hh]arry", "Harry"));
        System.out.println(alphanumeric.replaceAll("[^ej]", "X")); // Replace all that are not e or j
        System.out.println(alphanumeric.replaceAll("[abcdef345678]", "X"));
        System.out.println(alphanumeric.replaceAll("[a-f3-8]", "X"));

        System.out.println(alphanumeric.replaceAll("(?i)[a-f3-8]", "X")); // ignore case sensitivity
        System.out.println(alphanumeric.replaceAll("\\d", "X")); // Replace all digits
        System.out.println(alphanumeric.replaceAll("\\D", "X")); // Replace all non digits

        String hasWhitespace = "I have blanks and\ta tab, and also a newlines \n";
        System.out.println(hasWhitespace);
        System.out.println(hasWhitespace.replaceAll("\\s", "")); // Match all type of spaces
        System.out.println(hasWhitespace.replaceAll("\t", "X")); // Replace tabs
        System.out.println(hasWhitespace.replaceAll("\\S", ".")); // Replace all non whitespaces

        System.out.println(alphanumeric.replaceAll("\\w", "X")); // Replaces all a-z, 0-9 and _
        System.out.println(hasWhitespace.replaceAll("\\w", "X"));
        System.out.println(hasWhitespace.replaceAll("\\b", "X")); // wrap words

        // Quantifiers -> specifies how often a pattern repeats
        System.out.println(alphanumeric.replaceAll("^abcDeee", "YYY"));
        System.out.println(alphanumeric.replaceAll("^abcDe{3}", "YYY")); // e has to repeat 3 times to be a match
        System.out.println(alphanumeric.replaceAll("^abcDe+", "YYY")); // any amount of e
        String newAlphanumeric = "abcDF12GhhiiiiijabcDeeek1992";
        System.out.println(newAlphanumeric.replaceAll("^abcDe+", "YYY"));
        System.out.println(newAlphanumeric.replaceAll("^abcDe*", "YYY")); // followed by e or whatever else

        System.out.println(alphanumeric.replaceAll("^abcDe{2,5}", "YYY")); // e repeated from 2 to 5 times
        System.out.println(alphanumeric.replaceAll("h+i*j", "Y"));

        StringBuilder htmlText = new StringBuilder("<h1>My Heading</h1>");
        htmlText.append("<h2>Sub-heading</h2>");
        htmlText.append("<p>This is a paragraph about something.</p>");
        htmlText.append("<h2>Summary</h2>");
        htmlText.append("<p>Here is the summary.</p>");

//        String h2Pattern = ".*<h2>.*";
//        Pattern pattern = Pattern.compile(h2Pattern);
//        Matcher matcher = pattern.matcher(htmlText);
//        System.out.println(matcher.matches());

        String h2Pattern = "<h2>";
        Pattern pattern = Pattern.compile(h2Pattern);
        Matcher matcher = pattern.matcher(htmlText);
        matcher.reset();
        int count = 0;
        while(matcher.find()) {
            count++;
            System.out.println("Occurence " + count + " : " + matcher.start() + " to " + matcher.end());
        }

        String h2GroupPattern = "(<h2>.*</h2>)"; // Greedy quantifier
        Pattern grouPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = grouPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());
        groupMatcher.reset();

        while(groupMatcher.find()) {
            System.out.println("Ocurrence: " + groupMatcher.group(1));
        }

        String h2GroupPattern2 = "(<h2>.*?</h2>)"; // Lazy quantifier
        Pattern grouPattern2 = Pattern.compile(h2GroupPattern2);
        Matcher groupMatcher2 = grouPattern2.matcher(htmlText);
        System.out.println(groupMatcher2.matches());
        groupMatcher2.reset();

        while(groupMatcher2.find()) {
            System.out.println("Ocurrence: " + groupMatcher2.group(1));
        }

        System.out.println("**********************************");
        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPattern = Pattern.compile(h2TextGroups);
        Matcher h2TextMatcher = h2TextPattern.matcher(htmlText);
        while(h2TextMatcher.find()) {
            System.out.println("Occurrence: " + h2TextMatcher.group(1));
        }

        // Logical operators
        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));
        System.out.println("Harry".replaceAll("[H|h]arry", "Larry"));

        // not -> ^
        String tvTest = "tstvkt";
//        String tNotVRegExp = "t[^v]"; // t not followed by v but requires something to follow the t
        String tNotVRegExp = "t(?!v)"; // t that isn't followed by v -> ? is a look ahead
        Pattern tNotVPattern = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotVPattern.matcher(tvTest);

        count = 0;
        while(tNotVMatcher.find()) {
            count++;
            System.out.println("Occurrence " + count + " : " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        // US phone number example
        // regex: ^([\(]{1}[0-9]{3}[\)]{1}[ ]{1}[0-9]{3}[\-]{1}[0-9]{4})$ for (###) ###-####
        String phone1 = "1234567890";
        String phone2 = "(123) 456-7890)"; // should match
        String phone3 = "123 456-7890";
        String phone4 = "(123)456-7890";

        System.out.println("phone1 = " + phone1.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone2 = " + phone2.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone3 = " + phone3.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone4 = " + phone4.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

    }
}
