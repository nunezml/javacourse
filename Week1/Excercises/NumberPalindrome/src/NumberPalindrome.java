public class NumberPalindrome {

    public static boolean isPalindrome(int number) {
        int copyOfNumber = Math.abs(number);
        int reverse = 0;
        while (copyOfNumber > 0) {
            // extract the least-significant digit
            int digit = copyOfNumber % 10;
            reverse *= 10;
            reverse += digit;
            copyOfNumber /= 10;
        }

        return (reverse == copyOfNumber);
    }
}
