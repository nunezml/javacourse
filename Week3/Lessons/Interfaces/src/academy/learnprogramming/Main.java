package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        ITelephone timsPhone = new DeskPhone(123456);
        DeskPhone otherPhone = new DeskPhone(123123);
        timsPhone.powerOn();
        timsPhone.callPhone(123456);
        timsPhone.answer();

        timsPhone = new MobilePhone(2456789); // Can do as both are same type interface
        // otherPhone = new MobilePhone(2132132); // Can't do as they are not same type, even though both classes
                                                    // implement the same interface
        timsPhone.callPhone(2456789);
        timsPhone.powerOn();
        timsPhone.callPhone(2456789);
        timsPhone.answer();

    }
}
