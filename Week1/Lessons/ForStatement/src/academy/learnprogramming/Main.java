package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        System.out.println("10,000 at 2% interest = " + calculateInterest(10000.0, 2.0));
        System.out.println("10,000 at 3% interest = " + calculateInterest(10000.0, 3.0));
        System.out.println("10,000 at 4% interest = " + calculateInterest(10000.0, 4.0));
        System.out.println("10,000 at 5% interest = " + calculateInterest(10000.0, 5.0));

        // uent, call the calculateInterest method with the amount 10000 and interestRate of 2, 3, 4, 5, 6, 7 and 8
        // and print the results to the console window.
        for(int i=2; i<9; i++) {
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
        }
        // Backwards
        System.out.println("*************************");
        for(int i=8; i>=2; i--) {
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
        }
        // Prime number
        int range = 300;
        int primeCount = 0;
        for(int i=2;i<range;i++) {
            if (isPrime(i)) {
                System.out.println("Found prime number: " + i);
                primeCount++;
                if(primeCount == 3) break;
            }
        }
    }

    public static double calculateInterest(double amount, double interestRate) {
        return amount * (interestRate/100);
    }

    // Create a for statement using any range of numbers, determine if the number is a prime number using isPrime method
    // If it is a prime number, print it out AND increment a count of the number of prime numbers found
    // If that count is 3, exit the for loop
    //hint: Use the break; statement to exit
    public static boolean isPrime(int n) {
        if(n == 1) {
            return false;
        }

        for(int i=2; i <= (long) Math.sqrt(n); i++) {
            if(n % i == 0) {
                return false;
            }
        }

        return true;
    }
}
