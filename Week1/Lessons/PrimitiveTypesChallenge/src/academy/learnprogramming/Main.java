package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        byte myByte = 12;
        short myShort = 123;
        int myInt = 10230;
        long myLong = 50000L + 10L * (myByte + myShort + myInt);
        System.out.println(myLong);

        short shortTotal = (short) (1000 +10 * (myByte + myShort + myInt));
    }
}
