public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point() {

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance() {
        return Math.sqrt(((this.x)*(this.x) + (this.y)*(this.y)));
    }

    public double distance(int x, int y) {
        return Math.sqrt(((x-this.x)*(x-this.x) + (y-this.y)*(y-this.y)));
    }

    public double distance(Point point) {
        int pointX = point.getX();
        int pointY = point.getY();
        return Math.sqrt(((pointX-this.x)*(pointX-this.x) + (pointY-this.y)*(pointY-this.y)));
    }
}
