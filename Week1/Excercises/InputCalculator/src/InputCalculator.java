import java.util.Scanner;

public class InputCalculator {

    public static void inputThenPrintSumAndAverage() {
        Scanner scanner = new Scanner(System.in);

        int sum = 0;
        int count = 0;
        long average = 0;
        boolean isAnInt = scanner.hasNextInt();
        while(isAnInt) {
            sum += scanner.nextInt();
            count++;
            scanner.nextLine();
            isAnInt = scanner.hasNextInt();
        }
        average = (int) Math.round((float) sum / count);
        System.out.println("SUM = " + sum + " AVG = " + average);


    }
}
