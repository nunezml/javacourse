public class DiagonalStar {

    public static void printSquareStar(int number) {
        if(number >= 5) {
            String firstAndLastLine = "";
            for (int i = 0; i < number; i++) firstAndLastLine += "*";
            System.out.println(firstAndLastLine);

            int lineCount = 0;
            int regularization = 0;
            while (lineCount < number - 2) {
                String printLine = "*";
                // Calculate First and last spaces
                int wsFisrtLast;
                if (number % 2 == 0) {
                    // number is even
                    if (lineCount > (number - 2) / 2) {
                        regularization -= 2;
                        wsFisrtLast = lineCount + regularization;
                    } else if (lineCount == (number - 2) / 2) {
                        regularization = -1;
                        wsFisrtLast = lineCount + regularization;
                    } else {
                        wsFisrtLast = lineCount;
                    }
                } else {
                    // number is odd
                    if (lineCount > (number - 2) / 2) {
                        regularization -= 2;
                        wsFisrtLast = lineCount + regularization;
                    } else {
                        wsFisrtLast = lineCount;
                    }
                }
                // Add first spaces to string
                for (int i = 0; i < wsFisrtLast; i++) {
                    printLine += " ";
                }
                // Add first star
                printLine += "*";
                // Add middle spaces
                int middleSpace;
                if (number % 2 != 0 && lineCount == (number - 2) / 2) {
                    //number is odd and its middle line
                    middleSpace = 0;
                } else {
                    // number is even or its not odd in middle line, upper stage
                    middleSpace = number - 4 - wsFisrtLast*2;
                }
                // Add middle spaces to string
                for (int i = 0; i < middleSpace; i++) {
                    printLine += " ";
                }
                // Add second star
                if (number % 2 == 0 || lineCount != (number - 2) / 2) {
                    printLine += "*";
                }
                // Add last spaces to string
                for (int i = 0; i < wsFisrtLast; i++) {
                    printLine += " ";
                }
                // Add final star
                printLine += "*";
                // Update iterator
                lineCount++;
                System.out.println(printLine);
            }
            System.out.println(firstAndLastLine);
        } else {
            System.out.println("Invalid Value");
        }
    }
}
